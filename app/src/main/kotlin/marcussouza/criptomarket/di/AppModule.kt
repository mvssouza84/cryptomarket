package cineplayers.com.cineplayers.di

import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import marcussouza.criptomarket.BuildConfig
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideMoshi() = Moshi.Builder().build()


}