package marcussouza.criptomarket.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import marcussouza.criptomarket.ui.detail.DetailActivity
import marcussouza.criptomarket.ui.home.HomeActivity

@Module
abstract class ActivitieModule {

    @ContributesAndroidInjector
    abstract fun provideHomeActivityInjector(): HomeActivity

    @ContributesAndroidInjector
    abstract fun provideDetailActivityInjector(): DetailActivity
}