package marcussouza.criptomarket.util

import android.arch.lifecycle.MutableLiveData
import io.reactivex.Observable
import marcussouza.criptomarket.data.Resource
import marcussouza.criptomarket.ui.BaseViewModel

fun <T> Observable<T>.toNetWorkBoundResource() = NetworkBoundResource(this)

class NetworkBoundResource<T>(val observable: Observable<T>) {

    fun subscribeLiveData(viewModel: BaseViewModel, liveData: ResourceLiveData<T>) {
        viewModel.disposables.add(
                observable
                        .doOnError({ viewModel.loading.postValue(false) })
                        .subscribe(
                                { liveData.postValue(Resource.success(it)) },
                                { handleError(liveData, it) }))
    }

    private fun handleError(liveData: MutableLiveData<Resource<T>>, it: Throwable) {
        liveData.postValue(Resource.error(it))
    }
}