package marcussouza.criptomarket.data.api

import io.reactivex.Observable
import marcussouza.criptomarket.data.model.Coin
import marcussouza.criptomarket.data.model.Front
import retrofit2.http.GET
import retrofit2.http.Path

interface Api {

    @GET(value = "front")
    fun getFrontList(): Observable<List<Front>>

    @GET(value = "page/{coinId}")
    fun getCoinDetail(@Path("coinId") coinId: String): Observable<Coin>
}