package marcussouza.criptomarket.data


enum class Status { SUCCESS, ERROR }

data class Resource<out T>(
        val exception: Throwable?,
        val status: Status,
        val data: T?
) {
    companion object {
        fun <T> success(data: T?) = Resource(null, Status.SUCCESS, data)
        fun error(exception: Throwable?) = Resource(exception, Status.ERROR, null)
    }
}