package marcussouza.criptomarket.data.model

import android.annotation.SuppressLint
import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
data class Coin(
        @field:Json(name = "id") val id: String,
        @field:Json(name = "market_cap") val marketCap: Float,
        @field:Json(name = "display_name") val displayName: String,
        @field:Json(name = "volume") val volume: Float,
        @field:Json(name = "supply") val supply: Float,
        @field:Json(name = "cap24hrChange") val cap24hrChange: Float
) : Parcelable