package marcussouza.criptomarket.data.model

import android.annotation.SuppressLint
import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
data class Front(
        @field:Json(name = "perc") val percent: Float,
        @field:Json(name = "long") val longName: String,
        @field:Json(name = "short") val shortName: String,
        @field:Json(name = "price") val price: Float
) : Parcelable