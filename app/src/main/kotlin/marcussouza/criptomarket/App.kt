package marcussouza.criptomarket

import android.app.Activity
import android.app.Application
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import marcussouza.criptomarket.di.AppInjector
import marcussouza.criptomarket.di.DaggerAppComponent
import javax.inject.Inject


class App : Application(), HasActivityInjector {

    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()
        AppInjector.init(this, createAppComponent())
    }

    private fun createAppComponent() = DaggerAppComponent.builder()
            .application(this)
            .build()

    override fun activityInjector() = activityInjector
}