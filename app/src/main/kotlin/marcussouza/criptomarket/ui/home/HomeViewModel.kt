package marcussouza.criptomarket.ui.home

import marcussouza.criptomarket.data.api.Api
import marcussouza.criptomarket.data.model.Front
import marcussouza.criptomarket.ui.BaseViewModel
import marcussouza.criptomarket.util.ResourceLiveData
import marcussouza.criptomarket.util.toNetWorkBoundResource
import javax.inject.Inject

class HomeViewModel @Inject constructor(private val api: Api) : BaseViewModel() {

    val frontResource = ResourceLiveData<List<Front>>()

    fun getFrontList() {
        api.getFrontList()
                .doOnSubscribe { loading.postValue(true) }
                .doFinally { loading.postValue(false) }
                .toNetWorkBoundResource()
                .subscribeLiveData(this, frontResource)
    }
}