package marcussouza.criptomarket.ui.home

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_home.view.*
import marcussouza.criptomarket.R
import marcussouza.criptomarket.data.model.Front
import marcussouza.criptomarket.ui.BaseRecyclerAdapter

class HomeAdapter : BaseRecyclerAdapter<Front>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_home, parent, false),
            listener
    )

    override fun getItemCount() = recyclerList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).bind(recyclerList[position])
    }

    class ViewHolder(val view: View, val listener: OnItemClickListener?) : RecyclerView.ViewHolder(view), View.OnClickListener {

        init {
            view.itemHomeContainer.setOnClickListener(this)
        }

        fun bind(front: Front) = with(view) {
            itemHomeName.text = front.longName
            itemHomeSymbol.text = front.shortName
            itemHomePrice.text = resources.getString(R.string.price, front.price)
            itemHomePerc.text = resources.getString(R.string.perc, front.percent)
            if (front.percent > 0) {
                itemHomePerc.setTextColor(ContextCompat.getColor(context, android.R.color.holo_green_light))
            } else {
                itemHomePerc.setTextColor(ContextCompat.getColor(context, android.R.color.holo_red_light))
            }
            Picasso.with(context).load("https://coincap.io/images/coins/${front.longName}.png").into(itemHomeImage)
        }

        override fun onClick(v: View?) {
            listener?.onItemClick(view, adapterPosition)
        }
    }
}