package marcussouza.criptomarket.ui.home

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.Menu
import android.view.View
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.include_home_content.*
import kotlinx.android.synthetic.main.toolbar.*
import marcussouza.criptomarket.R
import marcussouza.criptomarket.data.model.Front
import marcussouza.criptomarket.ui.BaseActivity
import marcussouza.criptomarket.ui.BaseRecyclerAdapter
import marcussouza.criptomarket.ui.detail.DetailActivity
import marcussouza.criptomarket.ui.detail.DetailActivity.Companion.COIN_ID


class HomeActivity : BaseActivity(), BaseRecyclerAdapter.OnItemClickListener, SearchView.OnQueryTextListener {

    private val homeAdapter = HomeAdapter()
    private var oldList = ArrayList<Front>()

    private val viewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory)
                .get(HomeViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setupToolbar()
        setupRecycler()
        setupAnimation()
        setupObservers()
        viewModel.getFrontList()
    }

    private fun setupToolbar() {
        setSupportActionBar(activityHomeToolbar as Toolbar)
        supportActionBar?.title = ""
        toolbarTitle.text = resources.getString(R.string.home_title)
    }

    private fun setupRecycler() {
        val linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager
                .VERTICAL, false)
        includeHomeRecyclerView?.layoutManager = linearLayoutManager
        homeAdapter.listener = this
        includeHomeRecyclerView?.adapter = homeAdapter
    }

    private fun setupAnimation() {
    }

    private fun setupObservers() {
        viewModel.loading.observe(this, Observer {
            if (it == true) {
                activityHomeContainer.displayedChild = 0
            }
        })

        viewModel.frontResource.observeResource(this, onSuccess = {
            populateRecycler(it)
        }, onError = {
            Log.v("", it.exception.toString())
        })
    }

    private fun populateRecycler(it: List<Front>) {
        activityHomeContainer.displayedChild = 1
        oldList = it as ArrayList<Front>
        homeAdapter.addToList(oldList)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_home, menu)
        val myActionMenuItem = menu?.findItem(R.id.action_search)
        val searchView = myActionMenuItem?.actionView as SearchView
        searchView.setOnQueryTextListener(this)
        return true
    }

    override fun onItemClick(view: View, position: Int) {
        val intent = Intent(this, DetailActivity::class.java)
        intent.putExtra(COIN_ID, homeAdapter.getItem(position).shortName)
        startActivity(intent)
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        val newList = oldList.filter { it.longName.contains(newText.toString(), true) }
        homeAdapter.addToList(newList as ArrayList<Front>)
        return false
    }
}