package marcussouza.criptomarket.ui.detail

import marcussouza.criptomarket.data.api.Api
import marcussouza.criptomarket.data.model.Coin
import marcussouza.criptomarket.ui.BaseViewModel
import marcussouza.criptomarket.util.ResourceLiveData
import marcussouza.criptomarket.util.toNetWorkBoundResource
import javax.inject.Inject

class DetailViewModel @Inject constructor(private val api: Api) : BaseViewModel() {

    public val coinResource = ResourceLiveData<Coin>()

    public fun getCoin(coinId: String) {
        api.getCoinDetail(coinId)
                .doOnSubscribe { loading.postValue(true) }
                .doFinally { loading.postValue(false) }
                .toNetWorkBoundResource()
                .subscribeLiveData(this, coinResource)
    }
}