package marcussouza.criptomarket.ui.detail

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.constraint.ConstraintSet
import android.support.v4.content.ContextCompat
import android.support.v7.widget.Toolbar
import android.transition.ChangeBounds
import android.transition.TransitionManager
import android.view.animation.AnticipateOvershootInterpolator
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.include_coin_summary.*
import kotlinx.android.synthetic.main.include_detail_content_initial.*
import kotlinx.android.synthetic.main.toolbar.*
import marcussouza.criptomarket.R
import marcussouza.criptomarket.data.model.Coin
import marcussouza.criptomarket.ui.BaseActivity
import org.jetbrains.anko.doAsync

class DetailActivity : BaseActivity() {

    private lateinit var coinId: String

    private val viewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory)
                .get(DetailViewModel::class.java)
    }

    companion object {
        const val COIN_ID = "coinId"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        coinId = intent.extras.getString(COIN_ID)
        setupToolbar()
        setupObservers()
        viewModel.getCoin(coinId)
    }

    private fun setupToolbar() {
        setSupportActionBar(activityDetailToolbar as Toolbar)
        supportActionBar?.title = ""
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbarTitle.text = coinId
    }

    private fun setupObservers() {
        viewModel.loading.observe(this, Observer {
            if (it == true) {
                activityDetailContainer.displayedChild = 0
            }
        })

        viewModel.coinResource.observeResource(this, onSuccess = {
            populateView(it)
        }, onError = {

        })
    }

    private fun populateView(coin: Coin) {
        activityDetailContainer.displayedChild = 1
        activityDetailName.text = coin.displayName
        marketCapValue.text = resources.getString(R.string.price, coin.marketCap)
        volumeTodayValue.text = resources.getString(R.string.price, coin.volume)
        availableSupplyValue.text = resources.getString(R.string.price, coin.supply)
        Picasso.with(this).load("https://coincap.io/images/coins/${coin.displayName}.png").into(activityDetailImage)
        if (coin.cap24hrChange > 0) {
            includeCoinPercent.setTextColor(ContextCompat.getColor(this, android.R.color.holo_green_light))
            animatePositive(coin.cap24hrChange)
        } else {
            includeCoinPercent.setTextColor(ContextCompat.getColor(this, android.R.color.holo_red_light))
            animateNegative(coin.cap24hrChange)
        }
        doAsync {
            animateLayout()
        }
    }

    private fun animatePositive(cap24hrChange: Float) {
        includeCoinPercent.text = resources.getString(R.string.perc, cap24hrChange)
    }

    private fun animateNegative(cap24hrChange: Float) {
        includeCoinPercent.text = resources.getString(R.string.perc, cap24hrChange)
    }

    private fun animateLayout() {
        runOnUiThread {
            val constraintSet = ConstraintSet()
            constraintSet.clone(this, R.layout.include_detail_content)

            val transition = ChangeBounds()
            transition.interpolator = AnticipateOvershootInterpolator(3.0f)
            transition.duration = 1500

            TransitionManager.beginDelayedTransition(includeContainer, transition)

            constraintSet.applyTo(includeContainer)
        }
    }
}